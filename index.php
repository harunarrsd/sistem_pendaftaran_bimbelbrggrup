<?php
session_start();
include_once 'koneksi/koneksi.php';

if(isset($_SESSION['user'])!=""){
 header("Location: home.php");
}

if(isset($_POST['login'])){
 $user = mysql_real_escape_string($_POST['user']);
 $upass = mysql_real_escape_string($_POST['pass']);
 $res=mysql_query("SELECT * FROM login WHERE username='$user'");
 $row=mysql_fetch_array($res);
 
 if($row['password']==$upass){
  $_SESSION['user'] = $row['id'];
  header("Location: home.php");
}
 else{
  ?>
        <script type="text/javascript">
        alert("Username atau Password Salah!");
        </script>
        <?php
 }
 
}
?>

<!DOCTYPE html>
  <html>
    <head>
      <title>BrgGrup</title>

      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/brg.css"  media="screen,projection"/>

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body background="image/bk.jpg">
      <nav class="transparan" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo" style="font-family: Bernard MT Consened;font-size: 50pt;">BrgGrup!</a>
      <ul class="right hide-on-med-and-down">
            <li><a class="waves-effect waves-light modal-trigger" href="#modal2"><i class="material-icons left">perm_identity</i>Sign Up</a></li>
          </ul>
    </div>
  </nav>

<div class="tengah">
    <div class="black-text transparan border">
    <form method="POST">
    <br>
      <div class="container">
            <div class="input-field col s12 white-text">
              <i class="material-icons prefix">account_circle</i>
                <input placeholder="Nama Pengguna" id="username" name="user" type="text" class="validate" required />
            </div>
          </div>
          <div class="container">
            <div class="input-field col s12 white-text">
              <i class="material-icons prefix">lock</i>
                <input placeholder="Kata Sandi" id="password" name="pass" type="password" class="validate" required />
            </div>
          </div>
          <div class="container center">
            <button class="btn waves-effect waves-light white black-text" type="submit" name="login">login</button>
          </div>
          <br>
        </form>
    </div>
  </div>

  <div id="modal2" class="modal  yellow">
    <div class="modal-content black-text">
      <h4 align="center">SIGN UP </h4>
    </div>
    <div class="row">
   <form method="post" action="act-register.php" class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <input id="username" name="uname" type="text" class="validate" required />
          <label for="username" class="black-text">Username</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="password" name="password" type="password" class="validate" required />
          <label for="password" class="black-text">Password</label>
        </div>
      </div>
    </div>
    <div class="modal-footer yellow">
      <button class="btn waves-effect waves-light black" type="submit" name="btn-signup">SIGN UP
        <i class="material-icons right">send</i>
      </button>
    </div>
   </form>
  </div>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <div class="footer-copyright white">
    <div class="center black-text">© 2016 Copyright <a>Harun - Rekayasa Perangkat Lunak - XII RPL 2</a></div>
  </div>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script type="text/javascript" src="js/materialize.min.js"></script>
      <script>
      $('.button-collapse').sideNav({
        menuWidth: 300, // Default is 240
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
      );
      
      $('.modal-trigger').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
    }
  );
      </script>
    </body>
  </html>