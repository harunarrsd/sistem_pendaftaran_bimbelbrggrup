<!DOCTYPE html>
<html>
<head>
  <title>brgGrup</title>
      <link href="image/icon.png" rel="icon" type="image/x-icon"/>

      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/brg.css"  media="screen,projection"/>

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body background="image/home.jpg">
<nav role="navigation" class="transparan">
  <div class="nav-wrapper"><a id="logo-container" href="index.php" class="brand-logo" style="font-family: Bernard MT Consened;font-size: 50pt;">BrgGrup!</a>
    <ul class="right hide-on-med-and-down">
      <li><a class="waves-effect waves-light" href="home.php">Daftar</a></li>
      <li><a class="waves-effect waves-light dropdown-button transparan-putih" href="#" data-activates='dropdown1'>Lihat Pendaftaran</a></li>
      <ul id='dropdown1' class='dropdown-content transparan-putih'>
      <li><a href="lihat-sd.php" class="black-text">SD</a></li>
      <li class="divider"></li>
      <li><a href="lihat-smp.php" class="black-text">SMP</a></li>
      <li class="divider"></li>
      <li><a href="lihat-sma.php" class="black-text">SMA</a></li>
      </ul>
      <li><a class="waves-effect waves-light white-text" href="logout.php?logout">Sign Out</a></li>
    </ul>
  </div>
</nav>
<div class="container z-depth-2 transparan-putih">
<h4 class="center">Data Pendaftaran Bimbingan Belajar <a style="font-family:Bernard MT Consened" class="black-text">brgGrup</a></h4>
<table class="white centered responsive-table border container">
    <thead class="light-green lighten-2">
      <tr>
        <th data-field="no">No.</th>
        <th data-field="namaleng">Nama Lengkap</th>
        <th data-field="namapang">Nama Panggilan</th>
        <th data-field="tingkat">Tingkat</th>
        <th data-field="kelas">Kelas</th>
        <th data-field="sekolah">Sekolah</th>
        <th data-field="matpel">Mata Pelajaran</th>
      </tr>
    </thead>

<?php
    include('koneksi/koneksi.php');
    
    $query = mysql_query("SELECT * FROM siswa WHERE tingkat='SMP'") or die(mysql_error());
    
    if(mysql_num_rows($query) == 0){
      
      echo '<tr><td colspan="6">Tidak ada data!</td></tr>';
      
    }else{
      
      $no = 1;
      while($data = mysql_fetch_assoc($query)){
        
        echo '<tr>';
          echo '<td>'.$no.'</td>';
          echo '<td>'.$data['namalengkap'].'</td>';
          echo '<td>'.$data['namapanggilan'].'</td>';
          echo '<td>'.$data['tingkat'].'</td>'; 
          echo '<td>'.$data['kelas'].'</td>';
          echo '<td>'.$data['sekolah'].'</td>';
          echo '<td>'.$data['matpel'].'</td>';
        echo '</tr>';
        
        $no++;
        
      }
      
    }
    ?>
</table>
<br>
</div>

<br>
<div class="footer-copyright white">
  <div class="center black-text">© 2016 Copyright <a>Harun - Rekayasa Perangkat Lunak - XII RPL 2</a></div>
</div>

<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="js/materialize.min.js"></script>
<script>
  $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: true, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left' // Displays dropdown with edge aligned to the left of button
    }
  );
</script>
</body>
</html>