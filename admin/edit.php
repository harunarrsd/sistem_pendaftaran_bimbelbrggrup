<?php
session_start();
include_once '../koneksi/koneksi.php';

if(!isset($_SESSION['user']))
{
 header("Location: index.php");
}
$res=mysql_query("SELECT * FROM login WHERE id=".$_SESSION['user']);
$userRow=mysql_fetch_array($res);
?>
<!DOCTYPE html>
<html>
<head>
  <title>brgGrup</title>

      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="../css/brg.css"  media="screen,projection"/>

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body background="../image/home.jpg">
<nav role="navigation" class="transparan">
  <div class="nav-wrapper"><a id="logo-container" href="index.php" class="brand-logo" style="font-family: Bernard MT Consened;font-size: 50pt;">BrgGrup!</a>
    <ul class="right hide-on-med-and-down">
      <li><a class="waves-effect waves-light transparan-putih" href="tambah.php">Daftar</a></li>
      <li><a class="waves-effect waves-light dropdown-button" href="#" data-activates='dropdown1'>Lihat Pendaftaran</a></li>
      <ul id='dropdown1' class='dropdown-content transparan-putih'>
      <li><a href="lihat-sd.php" class="black-text">SD</a></li>
      <li class="divider"></li>
      <li><a href="lihat-smp.php" class="black-text">SMP</a></li>
      <li class="divider"></li>
      <li><a href="lihat-sma.php" class="black-text">SMA</a></li>
      </ul>
      <li><a class="waves-effect waves-light white-text" href="logout.php?logout">Sign Out</a></li>
    </ul>
  </div>
</nav>
<br>


<div class="container z-depth-2 transparan-putih">
  <p class="divider black"></p>
  <h4 class="center">Pendaftaran Siswa Bimbingan Belajar <a style="font-family: Bernard MT Consened;" class="black-text">brgGrup!</a></h4>
  <?php
  include('../koneksi/koneksi.php');
  
  $no = $_GET['id'];
  
  $show = mysql_query("SELECT * FROM siswa WHERE id='$no'");
  
  if(mysql_num_rows($show) == 0){
    
    echo '<script>window.history.back()</script>';
    
  }else{
  
    $data = mysql_fetch_assoc($show); 
  }
  ?>
  <div class="container">
    <div class="row">
    <form action="edit-proses.php" method="post">
    <input type="hidden" name="no" value="<?php echo $no; ?>">
      <div class="row">
        <div class="input-field col s12">
          <input type="text" class="validate" name="namaleng" value="<?php echo $data['namalengkap']; ?>">
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="text" class="validate" name="namapang" value="<?php echo $data['namapanggilan']; ?>">
        </div>
      </div>
      <div class="row">
        <br>
        <div class="input-field col s12">
          <select class="browser-default" name="tingkat" id="tingkat" onchange="changeValue2(this.value)" >
            <option value=0>-Pilih-</option>
            <?php
              include 'koneksi/koneksi.php';
              $result = mysql_query("select * from pembayaran");    
              $jsArray2 = "var tsekolah = new Array();\n";        
              while ($row = mysql_fetch_array($result)) {    
                  echo '<option value="' . $row['tingkat'] . '">' . $row['tingkat'] . '</option>';    
                  $jsArray2 .= "tsekolah['" . $row['tingkat'] . "'] = {bayar:'" . addslashes($row['spp']) . "'};\n";    
            }      
            ?>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="text" class="validate" name="kelas" value="<?php echo $data['kelas']; ?>">
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input type="text" class="validate" name="sekolah" value="<?php echo $data['sekolah']; ?>">
        </div>
      </div>
      <div class="row">
        <label class="black-text left">Mata Pelajaran</label>
        <br>
        <div class="input-field col s6">
          <select class="browser-default" name="matpel" id="matpel" onchange="changeValue(this.value)" >
            <option value=0>-Pilih-</option>
            <?php
              include 'koneksi/koneksi.php';
              $result = mysql_query("select * from matpel");    
              $jsArray = "var msekolah = new Array();\n";        
              while ($row = mysql_fetch_array($result)) {    
                  echo '<option value="' . $row['matpel'] . '">' . $row['matpel'] . '</option>';    
                  $jsArray .= "msekolah['" . $row['matpel'] . "'] = {ajar:'" . addslashes($row['guru']) . "'};\n";    
            }      
            ?>
          </select>
        </div>
        <div class="input-field col s6">
          <input type="text" class="validate black-text" name="guru" id="guru" disabled value="Pengajar">
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input type="text" class="validate black-text" name="spp" id="spp" disabled value="Pembayaran/Bulan">
        </div>
      </div>
      <div class="row">
        <button class="btn waves-effect waves-light right black" type="submit" name="edit">EDIT
        <i class="material-icons right">send</i>
        </button>
      </div>
    </form>
    </div>
  </div>
  <p class="divider black"></p>
</div>
<br>

<div class="footer-copyright white">
  <div class="container center black-text">© 2016 Copyright <a>Harun - Rekayasa Perangkat Lunak - XII RPL 2</a></div>
</div>

<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
 <script type="text/javascript" src="js/materialize.min.js"></script>
<script>

  <?php echo $jsArray; ?>  
    function changeValue(matpel){  
    document.getElementById('guru').value = msekolah[matpel].ajar; 
    };
    
  <?php echo $jsArray2; ?>  
    function changeValue2(tingkat){  
    document.getElementById('spp').value = tsekolah[tingkat].bayar; 
    };
</script>
</body>
</html>